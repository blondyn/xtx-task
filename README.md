### Choices made, and libraries used:
parcel - project builder. It's easy to use, lightweight and fast. Ideal for such small task.
react - facebook library i’m used to, can handle state easily, easy startup. Also provides compatibility between browsers.
smoothie.js - chart library, fast, lightweight, created for data streaming.

### Project structure:

- package.json - scripts for building the project,
- src/js - SPA components, writting using react.js,
- src/styles - base styles used on the website,
- src/index.html - entry point for project builder. It 'magically' looks for files associated with this file, and creates a build version.


### Things to improve:
I've chosen smoothie.js for charting, which is designed to present data streams. I think that wasn't the best choice, since library is lacking customisation options.
For instance, in order to 'feed' chart with data, i needed to look under the hood, and inject data there.
When you first open the chart, it flickers, until data series are stabilised.
The looks of it are not compelling enough.

Currently app is pooling data, every second. I believe that event based approach would fit better here. For instance SSE or WS.

Design is a bit flawed, I'm not really fluent in UI design, therefore it would take me much more time to implement it correctly.