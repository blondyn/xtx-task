import React from 'react';
import ReactDOM from 'react-dom';
import Page from './js/Page.js';

ReactDOM.render(<Page/>, document.getElementById('app'));
