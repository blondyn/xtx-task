import React from 'react';

export default ({ currencyName, buy, sell, mid, handleClick }) => (
    <tr onClick={handleClick(currencyName)}>
        <td>{currencyName}</td>
        <td>{buy}</td>
        <td>{sell}</td>
        <td>{mid}</td>
        <td><span>Details</span></td>
    </tr>
)
