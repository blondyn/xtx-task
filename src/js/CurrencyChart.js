import React from 'react';
import { SmoothieChart, TimeSeries } from 'smoothie';
import chartConfig from './chart-config';

const styles = {
    canvas: {
        margin: '42px'
    }
};

class CurrencyChart extends React.Component {

    seriesMid = null;
    seriesBuy = null;
    seriesSell = null;

    componentDidMount () {
        const canvas = this.refs.chart;

        const chart = new SmoothieChart(chartConfig);

        this.seriesMid = new TimeSeries();
        this.seriesBuy = new TimeSeries();
        this.seriesSell = new TimeSeries();

        // prefill data if available, hack
        this.seriesMid.data = this.preFillSeries('mid', this.props.history);
        this.seriesBuy.data = this.preFillSeries('buy', this.props.history);
        this.seriesSell.data = this.preFillSeries('sell', this.props.history);

        chart.addTimeSeries(this.seriesMid, { strokeStyle: 'rgba(0, 0, 255, 1)' });
        chart.addTimeSeries(this.seriesBuy, { strokeStyle: 'rgba(255, 0, 0, 1)' });
        chart.addTimeSeries(this.seriesSell, { strokeStyle: 'rgba(0, 255, 0, 1)' });

        chart.streamTo(canvas, 1000);
    }

    preFillSeries = (name, history) => {
        return history.map((history) => {
            const data = history[name];
            const timeStamp = history.time;
            return [
                timeStamp,
                data
            ];
        }).reverse();
    };

    componentWillUnmount () {
        this.smoothie = null;
        this.seriesMid = null;
        this.seriesBuy = null;
        this.seriesSell = null;
    }


    componentDidUpdate (prevProps) {
        const { mid, buy, sell } = prevProps.currency;
        this.seriesMid.append(Date.now(), mid);
        this.seriesBuy.append(Date.now(), buy);
        this.seriesSell.append(Date.now(), sell);
    }

    render () {
        return (
            <canvas
                style={styles.canvas}
                ref="chart"
                width={this.props.width}
                height={this.props.height}
            ></canvas>
        )
    }
}

CurrencyChart.defaultProps = {
    currency: {},
    width: '400px',
    height: '100px',
    history: []
};

export default CurrencyChart;