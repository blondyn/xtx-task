import React from 'react';
import Table from './Table.js';
import CurrencyPage from './CurrencyPage';
import '../styles/main.css';

const styles = {
    tableTitle: {
        display: 'flex',
        maxWidth: '600px',
        padding: '5px',
        fontSize: '30px',
        fontWeight: 400,
        textShadow: '-1px -1px 1px rgba(0, 0, 0, 0.1)',
        margin: '30px 0'
    },
    timeStamp: {
        textAlign: 'right',
        display: 'block',
        margin: '20px 0'
    }
};

class Page extends React.Component {

    url = 'https://xtx-web-challenge.herokuapp.com/live';
    state = {
        currencies: {},
        history: {},
        showModal: false
    };

    componentDidMount () {
        fetch(this.url)
            .then(res => res.json())
            .then(this.storeCurrencies)
            .catch(e => {
                console.error(e);
            });

        this.intervalId = setInterval(() => {
            fetch(this.url)
                .then(res => res.json())
                .then(this.storeCurrencies)

        }, 1000 /* ms */);
    }

    storeCurrencies = (currencies) => {
        this.setState(prevState => {
            const history = this.updateHistory(prevState);
            return {
                currencies,
                history,
                updateTime: Date.now()
            }
        })
    };

    updateHistory = (prevState) => {
        let nextHistory = {};

        const dataFetched = Object.keys(prevState.currencies).length >= 0;
        const historyEmpty = Object.keys(prevState.history).length === 0;

        if (dataFetched && historyEmpty) {
            // initialize history
            for (let [currency, values] of Object.entries(prevState.currencies)) {
                nextHistory[currency] = [{ time: prevState.updateTime, ...values }];
            }
        }

        for (const [currency, currencyHistory] of Object.entries(prevState.history)) {
            nextHistory[currency] = [{ time: prevState.updateTime, ...prevState.currencies[currency] }, ...currencyHistory];
            nextHistory[currency] = nextHistory[currency].slice(0, 60);
        }

        return nextHistory;
    };

    handleClick = (currencyName) => (evt) => {
        this.setState({
            showModal: true,
            actualCurrency: currencyName
        })
    };

    closeModal = (evt) => {
        this.setState({
            showModal: false
        });
    };

    componentWillUnmount () {
        this.intervalId = null;
    }

    render () {
        const history = this.state.history[this.state.actualCurrency];
        const currency = this.state.currencies[this.state.actualCurrency];

        return (
            <div>
                <h1 style={styles.tableTitle}>Currency market</h1>
                {this.state.showModal && <CurrencyPage
                    currencyName={this.state.actualCurrency}
                    currency={currency}
                    history={history}
                    closeModal={this.closeModal}
                />}
                <Table currencies={this.state.currencies}
                       handleClick={this.handleClick}
                />
                {this.state.updateTime &&
                <span style={styles.timeStamp}>Last update: {new Date(this.state.updateTime).toTimeString()}</span>}
            </div>
        )
    }
}

export default Page;