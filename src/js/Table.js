import React from 'react';
import CurrencyTableRow from './CurrencyRow';

const CurrencyTable = ({currencies, handleClick}) => (
    <table>
        <thead>
        <tr>
            <th>Currency</th>
            <th>buy</th>
            <th>sell</th>
            <th>mid</th>
        </tr>
        </thead>
        <tbody>
        {
            Object.keys(currencies).map((currencyName) => {
                const {buy, sell, mid} = currencies[currencyName];
                return (
                    <CurrencyTableRow key={currencyName}
                                      buy={buy}
                                      sell={sell}
                                      mid={mid}
                                      currencyName={currencyName}
                                      handleClick={handleClick}
                    />
                )
            })
        }
        </tbody>
    </table>
);

export default CurrencyTable;
