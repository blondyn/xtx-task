const timestampFormatter = (date) =>
    `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

export default {
    millisPerPixel: 150,
    maxValueScale: 1.01,
    minValueScale: 1.01,
    interpolation: 'line',
    grid: {
        fillStyle: '#111111',
        millisPerLine: 10000,
    },
    labels: {
        precision: 4,
    },
    tooltip: true,
    timestampFormatter
};
