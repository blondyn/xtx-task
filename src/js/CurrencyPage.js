import React from 'react';
import CurrencyChart from './CurrencyChart';

const styles = {
    container: {
        position: 'fixed',
        width: '100vw',
        height: '100vh',
        background: 'rgba(0,0,0,0.87)',
        color: '#00a4c0',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        left: 0
    },
    button: {
        backgroundColor: '#00a4c0',
        color: 'white',
        padding: '10px',
        border: 'none',
        position: 'absolute',
        top: 0,
        right: 0
    },
    inner: {
        maxWidth: 1280,
        position: 'relative'
    }
};

const CurrencyPage = ({ currencyName, currency, closeModal, history }) => {
    return (
        <div style={styles.container}>
            <div style={styles.inner}>
                <h1>{currencyName}</h1>
                <CurrencyChart
                    currency={currency}
                    history={history}
                    width={'600px'}
                    height={'300px'}
                />

                <button onClick={closeModal} style={styles.button}>
                    <div>Close</div>
                </button>
            </div>
        </div>
    )
};

export default CurrencyPage;